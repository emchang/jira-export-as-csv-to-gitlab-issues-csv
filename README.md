# Jira Export as CSV to GitLab Issues CSV

Exporting Jira issues and importing into GitLab issues will not work as-is. We need to modify the CSV to a GitLab-friendly format. This script is written as a temporary workaround for GitLab instances who are hit by this GitLab issue: [Increase timeout for importing Jira issues.](https://gitlab.com/gitlab-org/gitlab/-/issues/356814)

I wrote a Ruby script `scripts/convert.rb` to do the following:

- [x] Convert "Summary" column to "Title"
- [x] Add Description of the issue with more data
- [x] Add GitLab Actions such as labelling imported issues
- [x] Add All Comments into Description

## How to Convert

1. Select Jira issues and Export as CSV.
1. Create a label on the GitLab Project you want to import the issues.
2. Replace the source, destination and label variables in `scripts/convert.rb` to relevant values.
3. Run the script using `ruby convert.rb`.
4. [Import issues using CSV](https://docs.gitlab.com/ee/user/project/issues/csv_import.html) into the GitLab Project.

### Known Limitations

- The author of the issues will be the person who perform the import. There is no user mapping option.
- Attachments are not exported from Jira, hence it is unavailable when importing into GitLab issues.
