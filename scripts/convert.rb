#!/usr/bin/env ruby
require 'csv'

# Replace the following values accordingly
src_dir = "jira.csv"
dst_dir = "gitlabComments.csv"
label = "JIRA"

# Read the existing headers and replace the first header with "Title"
headers = CSV.read(src_dir, headers: true).headers
headers[0] = "Title"

# Read CSV
csv_data = CSV.parse(File.read(src_dir), headers: true)

# Merge comment values from all "Comment" columns into a single field
csv_data.each do |c|
  c["Summary"] = "[#{c["Issue key"]}] #{c["Summary"]}"
  
  # Initialize an array to store comment values
  merged_comment_values = c.map{|header, value| value if header.downcase == "comment" }.compact

  # Concatenate comment values with a space separator
  comments = merged_comment_values.join("\n")

  # Add relevant information to Description and apply labels
  c["Description"] = "#{c["Description"]} Issue metadata'\n'- **Issue Type:** #{c["Issue Type"]}'\n' - **Priority:** #{c["Priority"]}'\n' - **Comments:** #{comments} \n /label #{label}"
end

# Write the modified CSV data to the destination file
csv_out = CSV.open(dst_dir, "w", headers: true, write_headers: true) do |csv|
  csv << headers
  csv_data.each do |row|
    csv << row
  end
end
